var studconf = studconf || {};
studconf.session = {
	cookie_name: null,
	session_token: null,
	init: function(cookie_name) {
		this.cookie_name = cookie_name;
		if (Cookies.get(cookie_name)) {
			this.session_token = Cookies.get(cookie_name);
		}
	},
	logout: function() {
		studconf.socket.logout(this.session_token);
		this.session_token = null;
		Cookies.set(this.cookie_name, '')
	},
	destroy: function() {
		this.session_token = null;
		Cookies.set(this.cookie_name, '')
	},
	login: function(token) {
		this.session_token = token;
		Cookies.set(this.cookie_name, token);
	},
	isLoggedIn: function() {
		if (this.session_token != null) {
			return true;
		}
		return false;
	}
};