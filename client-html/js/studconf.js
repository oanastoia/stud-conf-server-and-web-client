// Global settings
var users,
	sessions,
	presenters,
	admins,
	studconf = studconf || {};
$(document).ready(function() {
	// init custom DOM controls
	studconf.dom.init();
	// init session (using cookie)
	studconf.session.init('studconf_token');
	// connect to socket
	studconf.socket.init('127.0.0.1', '8080');
	// subscribe to system socket events
	studconf.socket.subscribeSystemEvents();
	// subscribe to custom socket events
	studconf.socket.subscribeCustomEvents();
	// subscribe to DOM events (click)
	studconf.dom.subscribeDOMEvents();
	// check if admin is logged in and peform additional settings
	if (studconf.session.isLoggedIn()) {
		studconf.dom.loggedInView();
		studconf.socket.getUsers();
		studconf.socket.getSessions();
		studconf.socket.getPresenters();
		studconf.socket.getAdmins();
	}
});