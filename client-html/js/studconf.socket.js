var studconf = studconf || {};
studconf.socket = {
	socket: null,
	init: function(host, port) {
		this.socket = io.connect('http://' + host + ':' + port);
	},
	subscribeSystemEvents: function() {
		this.socket.on('connect', function() {
			studconf.dom.setStatus('online');
		});
		this.socket.on('connect_error', function(data) {
			studconf.dom.setStatus('offline');
		});
	},
	subscribeCustomEvents: function() {
		var context = this;
		this.socket.on('public:users-online', function(data) {
			// set users online
			studconf.dom.setOnlineUsers(data);
		});
		this.socket.on('admin:users', function(data) {
			users = data;
			studconf.dom.generateUsersList(users);
		});
		this.socket.on('admin:admins', function(data) {
			admins = data;
			studconf.dom.generateAdminsList(admins);
		});
		this.socket.on('admin:sessions', function(data) {
			sessions = data;
			studconf.dom.generateSessionsList(sessions);
		});
		this.socket.on('admin:presenters', function(data) {
			// save data in presenters
			presenters = data;
			studconf.dom.updateTypeaheadSources(presenters);
		});
		this.socket.on('admin:login', function(data) {
			studconf.session.login(data);
			studconf.dom.loggedInView();
			context.getUsers();
			context.getSessions();
			context.getPresenters();
			context.getAdmins();
		});
		this.socket.on('admin:session-expired', function(data) {
			studconf.session.destroy();
			studconf.dom.loggedOutView();
		});
		this.socket.on('admin:login-failed', function(data) {
			studconf.dom.setLoginError();
		});
	},
	logout: function(token) {
		this.socket.emit('admin:logout', {'token': token});
	},
	login: function(loginData) {
		this.socket.emit('admin:login', loginData);
	},
	getUsers: function() {
		this.socket.emit('admin:users', {
			'token': studconf.session.session_token
		});
	},
	getAdmins: function() {
		this.socket.emit('admin:admins', {
			'token': studconf.session.session_token
		});
	},
	getSessions: function() {
		this.socket.emit('admin:sessions', {
			'token': studconf.session.session_token
		});
	},
	getPresenters: function() {
		this.socket.emit('admin:presenters', {
			'token': studconf.session.session_token
		});
	},
	editUser: function(user) {
		// save data
		this.socket.emit('admin:user-edit', user);
	},
	editAdmin: function(admin) {
		// save data
		this.socket.emit('admin:admin-edit', admin);
	},
	editSession: function(session) {
		// save data
		this.socket.emit('admin:session-edit', session);
	},
	deleteUser: function(userid) {
		this.socket.emit('admin:user-delete', {
			'userid': '' + userid + '',
			'token': studconf.session.session_token
		});
	},
	deleteAdmin: function(adminid) {
		this.socket.emit('admin:admin-delete', {
			'adminid': '' + adminid + '',
			'token': studconf.session.session_token
		});
	},
	deleteSession: function(sessionid) {
		this.socket.emit('admin:session-delete', {
			'sessionid': '' + sessionid + '',
			'token': studconf.session.session_token
		});
	},
	addUser: function(user) {
		this.socket.emit('admin:user-save', user);
	},
	addAdmin: function(admin) {
		this.socket.emit('admin:admin-save', admin);
	},
	addSession: function(session) {
		this.socket.emit('admin:session-save', session);
	}
};