var studconf = studconf || {};
studconf.socket = {
	socket: null,
	init: function(host, port) {
		this.socket = io.connect('http://' + host + ':' + port);
	},
	subscribeSystemEvents: function() {
		this.socket.on('connect', function() {
			studconf.dom.setStatus('online');
		});
		this.socket.on('connect_error', function(data) {
			studconf.dom.setStatus('offline');
		});
	},
	subscribeCustomEvents: function() {
		var context = this;
		this.socket.on('public:users-online', function(data) {
			// set users online
			studconf.dom.setOnlineUsers(data);
		});
		this.socket.on('public:sessions', function(data) {
			sessions = data;
			studconf.dom.generateSessionsList(sessions);
			studconf.dom.generateDayFilterData(sessions);
		});
		this.socket.on('public:presenters', function(data) {
			// save data in presenters
			presenters = data;
			studconf.dom.generatePresentersList(presenters);
			studconf.dom.updateTypeaheadSources(presenters);
		});
	},
	getSessions: function() {
		this.socket.emit('public:sessions', {});
	},
	getPresenters: function() {
		this.socket.emit('public:presenters', {});
	}
};