var studconf = studconf || {};
studconf.dom = {
	init: function() {
		//will be used to init custom controls in filters
		$("#presenter-filter").typeahead({
			afterSelect: function(item) {
				$(".session-item").addClass("hide-by-presenter");
				$(".item-presenter-" + item.USERID).removeClass("hide-by-presenter");
			},
			displayText: function(item) {
				return item.NAME + " " + item.SURNAME;
			}
		});
	},
	subscribeDOMEvents: function() {
		//will be used to attach actions to filters and details pop-ups
		$("#presenter-filter").keyup(function(e) {
			if ($("#presenter-filter").val() == "") {
				$(".session-item").removeClass("hide-by-presenter");
			}
		});
		$("#keyword-filter").keyup(function(e) {
			var keyword = $("#keyword-filter").val();
			$(".session-item").addClass("hide-by-keyword");
			$.each(sessions, function(key, session) {
				if (session.SESSIONNAME.toLowerCase().indexOf(keyword.toLowerCase()) > -1) {
					$("tr[data-sessionid=\"" + session.SESSIONID + "\"]").removeClass("hide-by-keyword");
				}
			});
		});
		$("#filter-session-category input").click(function(e) {
			if ($(this).prop("checked") == true) {
				$(".panel-session-day tr.item-category-" + $(this).val()).removeClass("hide-by-session");
			} else {
				$(".panel-session-day tr.item-category-" + $(this).val()).addClass("hide-by-session");
			}
		});
	},
	setStatus: function(status) {
		switch (status) {
			case 'online':
				// set status icon
				$("#users-online-container").removeClass('label-default');
				$("#users-online-container").removeClass('label-danger');
				$("#users-online-container").addClass('label-success');
				break;
			case 'offline':
				// if we lose connection, set status icon red
				$("#users-online-container").removeClass('label-success');
				$("#users-online-container").addClass('label-danger');
				break;
			default:
				// if we lose connection, set status icon red
				$("#users-online-container").removeClass('label-success');
				$("#users-online-container").addClass('label-danger');
		}
	},
	setOnlineUsers: function(count) {
		$('#users-online').html(count);
	},
	generateSessionsList: function(sessions) {
		// TODO: Remove handlers to avoid memory leaks
		$(".panel-session-day").remove();
		// populate list in DOM
		var activeday = -1;
		$.each(sessions, function(key, session) {
			var starttime = moment.unix(session.STARTTIME).utc(),
				endtime = moment.unix(session.ENDTIME).utc(),
				day = starttime.get("dayOfYear");
			if (activeday != day) {
				$("#calendar").append("<div class=\"panel panel-primary panel-session-day panel-session-day-" + starttime.format("MMMM") + starttime.format("Do") + "\"><div class=\"panel-heading\"><h3 class=\"panel-title\">" + starttime.format("YYYY") + " " + starttime.format("MMMM") + " " + starttime.format("Do") + "</h3></div><div class=\"panel-body\"></div><table class=\"table\" id=\"sessions-list-" + day + "\"></table></div>");
				activeday = day;
			}
			$("#sessions-list-" + activeday).append("<tr class=\"session-item item-presenter-" + session.PRESENTERID + " item-category-" + session.CATEGORYID + "\" data-sessionid=\"" + session.SESSIONID + "\"><td class=\"session-time\">" + starttime.format("HH:mm") + " - " + endtime.format("HH:mm") + "</td><td class=\"session-type session-color-0" + session.CATEGORYID + "\"></td><td class=\"session-description\"><div class=\"session-title\">" + session.SESSIONNAME + "</div><div class=\"session-presenter\">" + session.NAME + " " + session.SURNAME + "</div></td></tr>");
		});
		$("#calendar table tr").click(function(e) {
			var sessionid = $(this).attr("data-sessionid"),
				sessiondetails;
			$.each(sessions, function(key,session) {
				if (sessionid == session.SESSIONID) {
					sessiondetails = session;
				}
			});
			$("#session-header").html(sessiondetails.SESSIONNAME + "<br/><small>by " + sessiondetails.NAME + " " + sessiondetails.SURNAME + "</small>");
			$("#session-description").html(sessiondetails.DESCRIPTION);
			$("#popup-session").modal("show");
		});
	},
	generateDayFilterData: function(sessions) {
		// TODO: Remove handlers to avoid memory leaks
		$("#filter-session-day .checkbox-inline").remove();
		// populate list in DOM
		var activeday = -1;
		$.each(sessions, function(key, session) {
			var starttime = moment.unix(session.STARTTIME).utc(),
				day = starttime.get("dayOfYear");
			if (activeday != day) {
				$("#filter-session-day").append("<label class=\"checkbox-inline\"><input type=\"checkbox\" checked=\"checked\" value=\"" + starttime.format("MMMM") + starttime.format("Do") + "\">" + starttime.format("MMMM") + " " + starttime.format("Do") + "</label>");
				activeday = day;
			}
		});
		$("#filter-session-day .checkbox-inline input").click(function(e) {
			if ($(this).prop("checked") == true) {
				$(".panel-session-day-" + $(this).val()).show();
			} else {
				$(".panel-session-day-" + $(this).val()).hide();
			}
		});
	},
	generatePresentersList: function(presenters) {
		// cleanup previous list
		$("#presenters-list").html("");
		// populate list in DOM
		$.each(presenters, function(key, presenter) {
			$("#presenters-list").append("<div class=\"public-presenter well\" data-presenterid=\"" + presenter.USERID + "\"><h4><span class=\"glyphicon glyphicon-user\"></span> " + presenter.NAME + " " + presenter.SURNAME + "<br/><small>" + presenter.JOBTITLE + "</small></h4><address><a href=\"mailto:" + presenter.EMAIL + "\">" + presenter.EMAIL + "</a><br/>Co: " + presenter.COMPANY + "</address></div>");
		});
	},
	updateTypeaheadSources: function(presenters) {
		// update source for presenters when data received from server
		$("#presenter-filter").data('typeahead').source = presenters;
	}
};