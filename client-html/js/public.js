// Global settings
var sessions,
    presenters;
$(document).ready(function() {
    // init custom DOM controls
    studconf.dom.init();
    // connect to socket
    studconf.socket.init('127.0.0.1', '8080');
    // subscribe to system socket events
    studconf.socket.subscribeSystemEvents();
    // subscribe to custom socket events
    studconf.socket.subscribeCustomEvents();
    // subscribe to DOM events (click)
    studconf.dom.subscribeDOMEvents();
    studconf.socket.getSessions();
    studconf.socket.getPresenters();
});
