var studconf = studconf || {};
studconf.dom = {
	init: function() {
		// datetime controls
		$('#add-session-start').datetimepicker({
			sideBySide: true,
			stepping: 15
		});
		$('#add-session-end').datetimepicker({
			sideBySide: true,
			stepping: 15
		});
		$('#edit-session-start').datetimepicker({
			sideBySide: true,
			stepping: 15
		});
		$('#edit-session-end').datetimepicker({
			sideBySide: true,
			stepping: 15
		});
		// typeahead presenter input
		$("#add-session-presenter").typeahead({
			displayText: function(item) {
				return item.NAME + " " + item.SURNAME;
			}
		});
		$("#edit-session-presenter").typeahead({
			displayText: function(item) {
				return item.NAME + " " + item.SURNAME;
			}
		});
	},
	subscribeDOMEvents: function() {
		// DOM handlers
		$("#login-button").click(function(e) {
			var loginData = {};
			loginData.user = $("#login-user").val();
			loginData.pass = $("#login-pass").val();
			studconf.socket.login(loginData);
		});
		$("#login-pass").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				var loginData = {};
				loginData.user = $("#login-user").val();
				loginData.pass = $("#login-pass").val();
				studconf.socket.login(loginData);
			}
		});
		$("#login-user").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				var loginData = {};
				loginData.user = $("#login-user").val();
				loginData.pass = $("#login-pass").val();
				studconf.socket.login(loginData);
			}
		});
		$("#open-user-add").click(function(e) {
			$("#popup-add-user").modal("show");
		});
		$("#open-session-add").click(function(e) {
			$("#popup-add-session").modal("show");
		});
		$("#open-admins-add").click(function(e) {
			$("#popup-add-admins").modal("show");
		});
		$("#popup-edit-user form").validator().on("submit",function(e) {
			if (e.isDefaultPrevented()) {
				return;
			}
			// gather data
			var userdetails = {};
			userdetails.token = studconf.session.session_token;
			userdetails.userid = $("#edit-user-userid").val();
			userdetails.jobtitle = $("#edit-user-jobtitle").val();
			userdetails.firstname = $("#edit-user-name").val();
			userdetails.surname = $("#edit-user-surname").val();
			userdetails.email = $("#edit-user-email").val();
			userdetails.company = $("#edit-user-company").val();
			if ($("#edit-user-presenter-yes").prop("checked") == true) {
				userdetails.presenter = "1";
			} else {
				userdetails.presenter = "0";
			}
			studconf.socket.editUser(userdetails);
			$("#edit-user-presenter-yes").prop('checked', false);
			$("#edit-user-presenter-no").prop('checked', true);
			// hide popup
			$("#popup-edit-user").modal("hide");
			e.preventDefault();
		});
		$("#popup-edit-admin form").validator().on("submit",function(e) {
			if (e.isDefaultPrevented()) {
				return;
			}
			// gather data
			var admin = {};
			admin.token = studconf.session.session_token;
			admin.adminid = $("#edit-admin-adminid").val();
			admin.username = $("#edit-admin-username").val();
			admin.password = $("#edit-admin-pass").val();
			studconf.socket.editAdmin(admin);
			// hide popup
			$("#popup-edit-admin").modal("hide");
			e.preventDefault();
		});
		$("#popup-edit-session form").validator().on("submit",function(e) {
			if (e.isDefaultPrevented()) {
				return;
			}
			if (typeof $("#edit-session-presenter").typeahead("getActive") != "undefined") {
				// gather data
				var sessiondetails = {};
				sessiondetails.token = studconf.session.session_token;
				sessiondetails.sessionname = $("#edit-session-name").val();
				sessiondetails.sessionstarttime = moment.utc($("#edit-session-start>input").val()).unix();
				sessiondetails.sessionendtime = moment.utc($("#edit-session-end>input").val()).unix();
				sessiondetails.sessiondescription = $("#edit-session-description").val();
				for (var i = 1; i <= 4; i++) {
					if ($("#edit-session-category-" + i).prop("checked") == true) {
						sessiondetails.sessioncategoryid = i;
						break;
					}
				}
				sessiondetails.sessionid = $("#edit-session-sessionid").val();
				sessiondetails.sessionpresenter = $("#edit-session-presenter").typeahead("getActive").USERID;
				studconf.socket.editSession(sessiondetails);
				// cleanup popup
				$("#edit-session-name").val('');
				$("#edit-session-start>input").val('');
				$("#edit-session-end>input").val('');
				$("#edit-session-start").data("DateTimePicker").date(new Date());
				$("#edit-session-end").data("DateTimePicker").date(new Date())
				$("#edit-session-description").val('');
				$("#edit-session-presenter").val('');
				$("#edit-session-category-1").prop('checked', true);
				$("#edit-session-category-2").prop('checked', false);
				$("#edit-session-category-3").prop('checked', false);
				$("#edit-session-category-5").prop('checked', false);
				$("#popup-edit-session").modal("hide");
			}
			e.preventDefault();
		});
		$("#delete-user-button").click(function(e) {
			studconf.socket.deleteUser($(this).attr("data-userid"));
		});
		$("#delete-admin-button").click(function(e) {
			studconf.socket.deleteAdmin($(this).attr("data-adminid"));
		});
		$("#delete-session-button").click(function(e) {
			studconf.socket.deleteSession($(this).attr("data-sessionid"));
		});
		$("#popup-add-admins form").validator().on("submit", function(e) {
			if (e.isDefaultPrevented()) {
    			return;
  			}
			// gather data
			var admin = {};
			admin.token = studconf.session.session_token;
			admin.username = $("#add-admin-username").val();
			admin.password = $("#add-admin-pass").val();
			studconf.socket.addAdmin(admin);
			// cleanup popup
			$("#add-admin-username").val('');
			$("#add-admin-pass").val('');
			$("#add-admin-pass-confirm").val('');
			// hide popup
			$("#popup-add-admins").modal("hide");
			e.preventDefault();
		});
		$("#popup-add-user form").validator().on("submit", function(e) {
			if (e.isDefaultPrevented()) {
				return;
			}
			// gather data
			var userdetails = {};
			userdetails.token = studconf.session.session_token;
			userdetails.jobtitle = $("#add-user-jobtitle").val();
			userdetails.firstname = $("#add-user-name").val();
			userdetails.surname = $("#add-user-surname").val();
			userdetails.email = $("#add-user-email").val();
			userdetails.company = $("#add-user-company").val();
			if ($("#add-user-presenter-yes").prop("checked") == true) {
				userdetails.presenter = "1";
			} else {
				userdetails.presenter = "0";
			}
			studconf.socket.addUser(userdetails);
			// cleanup popup
			$("#add-user-jobtitle").val('');
			$("#add-user-name").val('');
			$("#add-user-surname").val('');
			$("#add-user-email").val('');
			$("#add-user-company").val('');
			$("#add-user-presenter-yes").prop('checked', false);
			$("#add-user-presenter-no").prop('checked', true);
			// hide popup
			$("#popup-add-user").modal("hide");
			e.preventDefault();
		});
		$("#popup-add-session form").validator().on("submit", function(e) {
			if (e.isDefaultPrevented()) {
				return;
			}
			if (typeof $("#add-session-presenter").typeahead("getActive") != "undefined") {
				// gather data
				var sessiondetails = {};
				sessiondetails.token = studconf.session.session_token;
				sessiondetails.sessionname = $("#add-session-name").val();
				sessiondetails.sessionstarttime = moment.utc($("#add-session-start>input").val()).unix();
				sessiondetails.sessionendtime = moment.utc($("#add-session-end>input").val()).unix();
				sessiondetails.sessiondescription = $("#add-session-description").val();
				for (var i = 1; i <= 4; i++) {
					if ($("#add-session-category-" + i).prop("checked") == true) {
						sessiondetails.sessioncategoryid = i;
						break;
					}
				}
				sessiondetails.sessionpresenter = $("#add-session-presenter").typeahead("getActive").USERID;
				// save data
				studconf.socket.addSession(sessiondetails);
				// cleanup popup
				$("#add-session-name").val('');
				$("#add-session-start>input").val('');
				$("#add-session-end>input").val('');
				$("#add-session-description").val('');
				$("#add-session-presenter").val('');
				$("#add-session-category-1").prop('checked', true);
				$("#add-session-category-2").prop('checked', false);
				$("#add-session-category-3").prop('checked', false);
				$("#add-session-category-5").prop('checked', false);
				$("#popup-add-session").modal("hide");
			}
			e.preventDefault();		
		});
		$("#logout-button").click(function(e) {
			studconf.session.logout();
			studconf.dom.loggedOutView();
		});
	},
	loggedInView: function() {
		$("#logged-out-content").hide();
		$("#logged-in-content").show();
		$("#logout-button").show();
	},
	loggedOutView: function() {
		// on logout switch view and cleanup lists
		$("#logged-out-content").show();
		$("#logged-in-content").hide();
		$("#logout-button").hide();
		// logged out content clear
		$("#users-list").html("");
	},
	updateTypeaheadSources: function(presenters) {
		// update source for presenters when data received from server
		$("#add-session-presenter").data('typeahead').source = presenters;
		$("#edit-session-presenter").data('typeahead').source = presenters;
	},
	setLoginError: function () {
		$(".login-group").addClass('has-error has-feedback');
	},
	setStatus: function(status) {
		switch (status) {
			case 'online':
				// set status icon
				$("#users-online-container").removeClass('label-default');
				$("#users-online-container").removeClass('label-danger');
				$("#users-online-container").addClass('label-success');
				break;
			case 'offline':
				// if we lose connection, set status icon red
				$("#users-online-container").removeClass('label-success');
				$("#users-online-container").addClass('label-danger');
				break;
			default:
				// if we lose connection, set status icon red
				$("#users-online-container").removeClass('label-success');
				$("#users-online-container").addClass('label-danger');
		}
	},
	setOnlineUsers: function(count) {
		$('#users-online').html(count);
	},
	generateUsersList: function(users) {
		// cleanup previous list
		$("#users-list").html("");
		// add header
		$("#users-list").append("<tr><th>User Id</th><th>Job Title</th><th>Name</th><<th>Surname</th><<th>Email</th><th>Company</th><th>User type</th></tr>");
		// populate list in DOM
		$.each(users, function(key, user) {
			$("#users-list").append("<tr " + (user.ISPRESENTER == "1" ? "class=\"presenter-row\"" : "") + " data-userid=\"" + user.USERID + "\"><td>" + user.USERID + "</td><td>" + user.JOBTITLE + "</td><<td>" + user.NAME + "</td><<td>" + user.SURNAME + "</td><<td>" + user.EMAIL + "</td><td>" + user.COMPANY + "</td><td>" + (user.ISPRESENTER == "1" ? "Presenter" : "Attendee") + "</td></tr>")
		});
		$("#users-list tr").click(function(e) {
			var userid = $(e.currentTarget).attr("data-userid"),
				userdetails;
			$.each(users, function(key, user) {
				if (user.USERID == userid) {
					userdetails = user;
				}
			});
			$("#edit-user-userid").val(userdetails.USERID);
			$("#edit-user-jobtitle").val(userdetails.JOBTITLE);
			$("#edit-user-name").val(userdetails.NAME);
			$("#edit-user-surname").val(userdetails.SURNAME);
			$("#edit-user-email").val(userdetails.EMAIL);
			$("#edit-user-company").val(userdetails.COMPANY);
			$("#delete-user-button").attr("data-userid", userdetails.USERID);
			if (userdetails.ISPRESENTER) {
				$("#edit-user-presenter-yes").prop('checked', true);
				$("#edit-user-presenter-no").prop('checked', false);
			} else {
				$("#edit-user-presenter-yes").prop('checked', false);
				$("#edit-user-presenter-no").prop('checked', true);
			}
			$("#popup-edit-user").modal("show");
		});
	},
	generateAdminsList: function(admins) {
		// cleanup previous list
		$("#admins-list").html("");
		// add header
		$("#admins-list").append("<tr><th>Admin Id</th><th>Username</th></tr>");
		// populate list in DOM
		$.each(admins, function(key, admin) {
			$("#admins-list").append("<tr data-adminid=\"" + admin.ID + "\"><td>" + admin.ID + "</td><td>" + admin.USERNAME + "</td></tr>")
		});
		$("#admins-list tr").click(function(e) {
			var adminid = $(e.currentTarget).attr("data-adminid"),
				admindetails;
			$.each(admins, function(key, admin) {
				if (admin.ID == adminid) {
					admindetails = admin;
				}
			});
			$("#edit-admin-adminid").val(admindetails.ID);
			$("#edit-admin-username").val(admindetails.USERNAME);
			$("#edit-admin-pass").val('');
			$("#edit-admin-pass-confirm").val('');
			$("#delete-admin-button").attr("data-adminid", admindetails.ID);
			$("#popup-edit-admin").modal("show");
		});
	},
	generateSessionsList: function(sessions) {
		// TODO: Remove handlers to avoid memory leaks
		$(".panel-session-day").remove();
		// populate list in DOM
		var activeday = -1;
		$.each(sessions, function(key, session) {
			var starttime = moment.unix(session.STARTTIME).utc(),
				endtime = moment.unix(session.ENDTIME).utc(),
				day = starttime.get("dayOfYear");
			if (activeday != day) {
				$("#calendar").append("<div class=\"panel panel-primary panel-session-day\"><div class=\"panel-heading\"><h3 class=\"panel-title\">" + starttime.format("YYYY") + " " + starttime.format("MMMM") + " " + starttime.format("Do") + "</h3></div><div class=\"panel-body\"></div><table class=\"table\" id=\"sessions-list-" + day + "\"></table></div>");
				activeday = day;
			}
			$("#sessions-list-" + activeday).append("<tr class=\"session-item\" data-sessionid=\"" + session.SESSIONID + "\"><td class=\"session-time\">" + starttime.format("HH:mm") + " - " + endtime.format("HH:mm") + "</td><td class=\"session-type session-color-0" + session.CATEGORYID + "\">Technical</td><td class=\"session-description\"><div class=\"session-title\">" + session.SESSIONNAME + "</div><div class=\"session-presenter\">" + session.NAME + " " + session.SURNAME + "</div></td></tr>");
		});
		// append DOM actions
		$("#calendar table tr").click(function(e) {
			// process details
			var sessionid = $(e.currentTarget).attr("data-sessionid"),
				sessiondetails = {},
				// Dummy date in case the presenter is null (added manually, or bad session)
				presenterdetails = {
					NAME: "",
					SURNAME: ""
				},
				stime,
				etime;
			$.each(sessions, function(key, value) {
				if (value.SESSIONID == sessionid) {
					sessiondetails = value;
				}
			});
			$.each(presenters, function(key, value) {
				if (value.USERID == sessiondetails.PRESENTERID) {
					presenterdetails = value;
				}
			});
			stime = moment.unix(sessiondetails.STARTTIME).utc();
			etime = moment.unix(sessiondetails.ENDTIME).utc();
			$("#edit-session-name").val(sessiondetails.SESSIONNAME);
			$("#edit-session-sessionid").val(sessionid);
			$("#delete-session-button").attr("data-sessionid", sessionid);
			$("#edit-session-start").data("DateTimePicker").date(stime);
			$("#edit-session-end").data("DateTimePicker").date(etime);
			$("#edit-session-description").val(sessiondetails.DESCRIPTION);
			$("#edit-session-presenter").val(presenterdetails.NAME + " " + presenterdetails.SURNAME);
			$("#edit-session-presenter").data('active', presenterdetails);
			$("#edit-session-category-1").prop('checked', false);
			$("#edit-session-category-2").prop('checked', false);
			$("#edit-session-category-3").prop('checked', false);
			$("#edit-session-category-5").prop('checked', false);
			$("#edit-session-category-" + sessiondetails.CATEGORYID).prop('checked', true);
			$("#popup-edit-session").modal("show");
		});
	}
};