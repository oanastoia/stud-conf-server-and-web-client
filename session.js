var crypto = require('crypto'); // used to generate authentication tokens
module.exports = {
    session_tokens: {},
    isValid: function(action, token, socket) {
    	console.log("[%s] [Session] [action=%s] Validation [token=%s]".debug, socket.id, action, token);
        var token_found = false;
        if (typeof(token) != "undefined") {
            // search token in all sessions to see if we find a match (meaning new session with same token, but different id)
            for (sesstoken in this.session_tokens) {
                if (token == this.session_tokens[sesstoken]) {
                    token_found = true;
                    socket.join('admins');
                }
            }
        }
        return token_found;
    },
    create: function(socket) {
        var current_date = (new Date()).valueOf().toString();
        var random = Math.random().toString();
        this.session_tokens[socket.id] = crypto.createHash('sha1').update(current_date + random).digest('hex');
        socket.join('admins');
        console.log("[%s] [Socket] [room=%s] Created session [token=%s]".auth, socket.id, "admins", this.session_tokens[socket.id]);
        return this.session_tokens[socket.id];
    },
    destroy: function(socket, data) {
    	socket.leave('admins');
        if (typeof(data.token) != "undefined") {
            for (sesstoken in this.session_tokens) {
                if (data.token == this.session_tokens[sesstoken]) {
                	this.session_tokens[sesstoken] = null;
                }
            }
        }
    	console.log("[%s] [Session] Destroyed session [token=%s]".auth, socket.id, data.token);
    }
};