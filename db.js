var mysql = require('mysql'),
    stringify = require('json-stringify-safe'),
    db;
module.exports = {
    connect: function(config, timeoutcontext) {
        var context = timeoutcontext || this;
        db = mysql.createConnection(config);
        db.connect(function(err) {
            if (!err) {
                console.log("[System] Connected to Database (%s)".info, config.host);
            } else {
                console.log("[System] Error connecting to Database (%s)".error, config.host);
                setTimeout(context.connect, 2000, config, context); // reconnect if connection dropped
            }
        });
        db.on('error', function(err) {
            console.log("[Database] Error connecting to: %s".error, err);
            if (err.code === 'PROTOCOL_CONNECTION_LOST') { 
                context.connect(); 
            } else { 
                throw err; 
            }
        });
    },
    adminGetUsers: function(socketid, callback) {
        console.log("[%s] [Database] Running adminGetUsers query".debug, socketid);
        db.query('select * from users order by SURNAME ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for adminGetUsers".debug, socketid);
            callback(rows);
        });
    },
    publicGetPresenters: function(socketid, callback) {
        console.log("[%s] [Database] Running publicGetPresenters query".debug, socketid);
        db.query('select * from users WHERE ISPRESENTER=1 order by SURNAME ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for publicGetPresenters".debug, socketid);
            callback(rows);
        });
    },
    adminGetAdmins: function(socketid, callback) {
        console.log("[%s] [Database] Running adminGetAdmins query".debug, socketid);
        db.query('select * from administrators order by USERNAME ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for adminGetAdmins".debug, socketid);
            callback(rows);
        });
    },
    adminGetPresenters: function(socketid, callback) {
        console.log("[%s] [Database] Running adminGetPresenters query".debug, socketid);
        db.query('select USERID, NAME, SURNAME from users WHERE ISPRESENTER=1 order by SURNAME ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for adminGetPresenters".debug, socketid);
            callback(rows);
        });
    },
    adminGetSessions: function(socketid, callback) {
        console.log("[%s] [Database] Running adminGetSessions query".debug, socketid);
        db.query('select UNIX_TIMESTAMP(CONVERT_TZ(START, "+00:00", @@global.time_zone)) AS STARTTIME, UNIX_TIMESTAMP(CONVERT_TZ(END, "+00:00", @@global.time_zone)) AS ENDTIME, SESSIONNAME, NAME, SURNAME, CATEGORYID, SESSIONID, DESCRIPTION, PRESENTERID from calendar left join users on calendar.PRESENTERID=users.USERID order by START ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for adminGetSessions".debug, socketid);
            callback(rows);
        });
    },
    adminAddUser: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminAddUser query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('insert into users (JOBTITLE, NAME, SURNAME, EMAIL, COMPANY, ISPRESENTER)  VALUES (' + db.escape(data.jobtitle) + ', ' + db.escape(data.firstname) + ', ' + db.escape(data.surname) + ', ' + db.escape(data.email) + ', ' + db.escape(data.company) + ', ' + db.escape(data.presenter) + ');', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetUsers".debug, socketid);
            context.adminGetUsers(socketid, callback);
        });
    },
    adminAddAdmin: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminAddAdmin query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('insert into administrators (USERNAME, PASSWORD)  VALUES (' + db.escape(data.username) + ', MD5(' + db.escape(data.password) + '));', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetAdmins".debug, socketid);
            context.adminGetAdmins(socketid, callback);
        });
    },
    adminAddSession: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminAddSession query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('insert into calendar (SESSIONNAME, START, END, DESCRIPTION, CATEGORYID, PRESENTERID)  VALUES (' + db.escape(data.sessionname) + ', CONVERT_TZ(FROM_UNIXTIME(' + db.escape(data.sessionstarttime) + '), @@global.time_zone, "+00:00"), CONVERT_TZ(FROM_UNIXTIME(' + db.escape(data.sessionendtime) + '), @@global.time_zone, "+00:00"), ' + db.escape(data.sessiondescription) + ', ' + db.escape(data.sessioncategoryid) + ', ' + db.escape(data.sessionpresenter) + ');', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetSessions".debug, socketid);
            context.adminGetSessions(socketid, callback);
        });
    },
    adminEditSession: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminEditSession query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('update calendar set SESSIONNAME = ' + db.escape(data.sessionname) + ', START = CONVERT_TZ(FROM_UNIXTIME(' + db.escape(data.sessionstarttime) + '), @@global.time_zone, "+00:00"), END = CONVERT_TZ(FROM_UNIXTIME(' + db.escape(data.sessionendtime) + '), @@global.time_zone, "+00:00"), DESCRIPTION = ' + db.escape(data.sessiondescription) + ', CATEGORYID = ' + db.escape(data.sessioncategoryid) + ', PRESENTERID =  ' + db.escape(data.sessionpresenter) + ' WHERE SESSIONID = ' + db.escape(data.sessionid) + ';', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetSessions".debug, socketid);
            context.adminGetSessions(socketid, callback);
        });
    },
    adminEditUser: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminEditUser query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('update users set JOBTITLE = ' + db.escape(data.jobtitle) + ', NAME = ' + db.escape(data.firstname) + ', SURNAME = ' + db.escape(data.surname) + ', EMAIL = ' + db.escape(data.email) + ', COMPANY = ' + db.escape(data.company) + ', ISPRESENTER = ' + db.escape(data.presenter) + ' where USERID = ' + db.escape(data.userid) + ';', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetUsers".debug, socketid);
            context.adminGetUsers(socketid, callback);
        });
    },
    adminEditAdmin: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminEditAdmin query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('update administrators set USERNAME = ' + db.escape(data.username) + ', PASSWORD = MD5(' + db.escape(data.password) + ') where ID = ' + db.escape(data.adminid) + ';', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetAdmins".debug, socketid);
            context.adminGetAdmins(socketid, callback);
        });
    },
    adminLogin: function(socketid, data, callback) {
        console.log("[%s] [Database] [user=%s] Running adminLogin query".auth, socketid, data.user);
        db.query('SELECT ID from administrators where USERNAME=' + db.escape(data.user) + ' and PASSWORD=MD5(' + db.escape(data.pass) + ');', function(err, rows, fields) {
            if (err) throw err;
            if (rows.length == 1) {
                console.log("[%s] [Database] [userid=%s] [username=%s] Data sent to callback for adminLogin".debug, socketid, rows[0].ID, data.user);
                callback(true);
            } else {
                console.log("[%s] [Database] Data sent to callback for adminLogin".warn, socketid);
                callback(false);
            }
        });
    },
    adminDeleteUser: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminDeleteUser query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('delete from users where USERID = ' + db.escape(data.userid), function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetUsers".debug, socketid);
            context.adminGetUsers(socketid, callback);
        });
    },
    adminDeleteAdmin: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminDeleteAdmin query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('delete from administrators where ID = ' + db.escape(data.adminid), function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded adminGetAdmins".debug, socketid);
            context.adminGetAdmins(socketid, callback);
        });
    },
    adminDeleteSession: function(socketid, data, callback) {
        console.log("[%s] [Database] Running adminDeleteSession query with [payload=%s]".info, socketid, stringify(data));
        var context = this;
        db.query('delete from calendar where SESSIONID = ' + db.escape(data.sessionid), function(err, rows, fields) {
            if (err) throw err;
            db.query('delete from pinnedsessions where SESSIONID = ' + db.escape(data.sessionid), function(err, rows, fields) {
                if (err) throw err;
                // delete from pinned sessions
                console.log("[%s] [Database] Data forwarded adminGetSessions".debug, socketid);
                context.adminGetSessions(socketid, callback);
            });
        });
    },
    getCalendar: function(socketid, callback) {
        console.log("[%s] [Database] Running getCalendar query".debug, socketid);
        db.query('select UNIX_TIMESTAMP(CONVERT_TZ(START, "+00:00", @@global.time_zone)) AS STARTTIME, UNIX_TIMESTAMP(CONVERT_TZ(END, "+00:00", @@global.time_zone)) AS ENDTIME, SESSIONNAME, CATEGORYID, SESSIONID, DESCRIPTION, PRESENTERID, NAME, SURNAME from calendar left join users on calendar.PRESENTERID=users.USERID ORDER BY START ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for getCalendar".debug, socketid);
            callback(rows);
        });
    },
    pinSession: function(socketid, data) {
        console.log("[%s] [Database] [ownerid=%s] Running pinSession query".debug, socketid, data.ownerid);
        db.query('select SESSIONID from pinnedsessions where OWNERID = ' + db.escape(data.ownerid) + ' AND SESSIONID = ' + db.escape(data.sessionid) + ';', function(err, rows, fields) {
            if (err) throw err;
            // check if contact (based on pair of ownerid and userid already in table)
            if (rows.length == 0) {
                // no contact found in table 
                db.query('insert into pinnedsessions (OWNERID, SESSIONID) VALUES(' + db.escape(data.ownerid) + ', ' + db.escape(data.sessionid) + ')', function(err, rows, fields) {
                    if (err) throw err;
                    console.log("[%s] [Database] [ownerid=%s] [sessionid=%s] Duplicate not found: Pinning session".debug, socketid, data.ownerid, data.sessionid);
                });
            } else {
                console.log("[%s] [Database] [ownerid=%s] [sessionid=%s] Session already pinned".debug, socketid, data.ownerid, data.sessionid);
            }
        });
    },
    unpinSession: function(socketid, data, callback) {
        console.log("[%s] [Database] [ownerid=%s] [sesssionid=%s] Running unpinSession query".debug, socketid, data.ownerid, data.sessionid);
        var context = this;
        db.query('delete from pinnedsessions where OWNERID = ' + db.escape(data.ownerid) + ' AND SESSIONID = ' + db.escape(data.sessionid) + ';', function(err, rows, fields) {
            if (err) throw err;
            // check if contact (based on pair of ownerid and userid already in table)
            console.log("[%s] [Database] Data forwarded getPinnedSessions".debug, socketid);
            context.getPinnedSessions(socketid, data, callback);
        });
    },
    getPinnedSessions: function(socketid, data, callback) {
        console.log("[%s] [Database] [owner=%s] Running getPinnedSessions query".debug, socketid, data.ownerid);
        db.query('select UNIX_TIMESTAMP(CONVERT_TZ(START, "+00:00", @@global.time_zone)) AS STARTTIME, UNIX_TIMESTAMP(CONVERT_TZ(END, "+00:00", @@global.time_zone)) AS ENDTIME, SESSIONNAME, CATEGORYID, SESSIONID, DESCRIPTION, PRESENTERID, NAME, SURNAME from calendar left join users on calendar.PRESENTERID=users.USERID where SESSIONID IN (select SESSIONID from pinnedsessions where OWNERID = ' + db.escape(data.ownerid) + ')  order by START ASC;', function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for getPinnedSessions".debug, socketid);
            callback(rows);
        });
    },
    saveContact: function(socketid, data, callback) {
        console.log("[%s] [Database] Running saveContact query".debug, socketid);
        var context = this;
        db.query('select OWNERID from savedcontacts where OWNERID = ' + db.escape(data.ownerid) + ' AND PARTICIPANTID = ' + db.escape(data.participantid) + ';', function(err, rows, fields) {
            if (err) throw err;
            // check if contact (based on pair of ownerid and userid already in table)
            if (rows.length == 0) {
                // no contact found in table 
                db.query('insert into savedcontacts (OWNERID, PARTICIPANTID)  VALUES (' + db.escape(data.ownerid) + ', ' + db.escape(data.participantid) + ');', function(err, rows, fields) {
                    if (err) throw err;
                    console.log("[%s] [Database] Data forwarded getSavedContacts".debug, socketid);
                    context.getSavedContacts(socketid, data.ownerid, callback);
                });
            }
        });
    },
    deleteContact: function(socketid, data, callback) {
        console.log("[%s] [Database] Running deleteContact query".debug, socketid);
        var context = this;
        db.query('delete from savedcontacts where OWNERID = ' + db.escape(data.ownerid) + ' and PARTICIPANTID = ' + db.escape(data.contactid), function(err, rows, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data forwarded getSavedContacts".debug, socketid);
            context.getSavedContacts(socketid, data.ownerid, callback);
        });
    },
    getSavedContacts: function(socketid, ownerid, callback) {
        console.log("[%s] [Database] Running getSavedContacts query".debug, socketid);
        db.query('select * from users where USERID IN (select PARTICIPANTID from savedcontacts where OWNERID = ' + db.escape(ownerid) + ') ORDER BY SURNAME ASC;', function(err, rows_contacts, fields) {
            if (err) throw err;
            console.log("[%s] [Database] Data sent to callback for getSavedContacts".debug, socketid);
            callback(rows_contacts);
        });
    }
}