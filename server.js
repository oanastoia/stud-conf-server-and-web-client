// Global config for the app
var config = {
	wifi: {
		networkSSID: "SecureGW01",
		networkPass: "alag0erd9"
	},
	db: {
        host: "52.10.204.133",
        user: "root",
        password: "alag0erd8",
        database: "StudConf"
    }
};

// Define dependencies and initialize global handlers for web server, sockets and mysql
var express = require('express'),
	app = express(),
	http = require('http'),
	server = http.createServer(app),
	io = require('socket.io').listen(server),
	session = require('./session'),
	dbase = require('./db'),
	stringify = require('json-stringify-safe'), // required to avoid circular dependency for big objects when outputting to json
	moment = require('moment'), // used for time processing in logs
	colors = require('colors'), // used to add colors to console.log
	logtime = require('log-timestamp')(function() {
		// used to add timestamps in console.log
		return '[' + moment().format("YY-MM-DD HH:mm:ss.SSS") + '] %s'
	});
colors.setTheme({
	auth: 'cyan',
	prompt: 'grey',
	info: 'green',
	warn: 'yellow',
	debug: 'grey',
	error: 'red'
});
dbase.connect(config.db);
// Rest server definition
// Expose on / the static content from client-html
app.use("/", express.static("./client-html/"));
// Socket server setup
io.sockets.on('connection', function(socket) {
	console.log("[%s] [Socket] [clientip=%s] Client connected".info, socket.id, socket.handshake.address);
	// Number of clients found in io.eio.clientsCount
	// Client Id's found in io.eio.clients
	// Broadcast [clients online on connection
	io.emit("public:users-online", io.eio.clientsCount);
	console.log("[%s] [System] [action=public:users-online] Broadcast".debug, socket.id);
	socket.on('disconnect', function(data) {
		console.log("[%s] [Socket] Client disconnected".info, socket.id);
		socket.broadcast.emit("public:users-online", io.eio.clientsCount);
		console.log("[%s] [Socket] [action=public:users-online] Broadcast".debug, socket.id);
	});
	socket.on("public:sessions", function(data) {
		console.log("[%s] [Socket] [action=public:sessions] Incoming".debug, socket.id);
		dbase.getCalendar(socket.id, function(data) {
			socket.emit("public:sessions", data);
			console.log("[%s] [Socket] [action=public:sessions] Outgoing".debug, socket.id);
		});
	});
	socket.on("public:presenters", function(data) {
		console.log("[%s] [Socket] [action=public:presenters] Incoming".debug, socket.id);
		dbase.publicGetPresenters(socket.id, function(data) {
			socket.emit("public:presenters", data);
			console.log("[%s] [Socket] [action=public:presenters] Outgoing".debug, socket.id);
		});
	});
	socket.on("user:sessions", function(data) {
		console.log("[%s] [Socket] [action=user:sessions] Incoming [%s]".debug, socket.id, stringify(data)); // data used for reconnect bug testing
		dbase.getPinnedSessions(socket.id, data, function(data) {
			socket.emit("user:sessions", data);
			console.log("[%s] [Socket] [action=user:sessions] Outgoing ".debug, socket.id);
		});
	});
	socket.on("user:session-pin", function(data) {
		console.log("[%s] [Socket] [action=user:session-pin] Incoming".debug, socket.id);
		dbase.pinSession(socket.id, data);
	});
	socket.on("user:session-unpin", function(data) {
		console.log("[%s] [Socket] [action=user:session-unpin] Incoming".debug, socket.id);
		dbase.unpinSession(socket.id, data, function(data) {
			socket.emit("user:sessions", data);
			console.log("[%s] [Socket] [action=user:sessions] Outgoing".debug, socket.id);
		});
	});
	socket.on("admin:users", function(data) {
		console.log("[%s] [Socket] [action=admin:users] Incoming".debug, socket.id);
		if (session.isValid("admin:users", data.token, socket)) {
			dbase.adminGetUsers(socket.id, function(data) {
				socket.emit("admin:users", data);
				console.log("[%s] [Socket] [action=admin:users] Outgoing".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:users] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:wifi", function(data) {
		console.log("[%s] [Socket] [action=admin:wifi] Incoming".debug, socket.id);
		if (session.isValid("admin:wifi", data.token, socket)) {
			socket.emit("admin:wifi", config.wifi);
			console.log("[%s] [Socket] [action=admin:wifi] Outgoing".debug, socket.id);
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:wifi] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:admins", function(data) {
		console.log("[%s] [Socket] [action=admin:admins] Incoming".debug, socket.id);
		if (session.isValid("admin:admins", data.token, socket)) {
			dbase.adminGetAdmins(socket.id, function(data) {
				socket.emit("admin:admins", data);
				console.log("[%s] [Socket] [action=admin:admin] Outgoing".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:admins] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:presenters", function(data) {
		console.log("[%s] [Socket] [action=admin:presenters] Incoming".debug, socket.id);
		if (session.isValid("admin:presenters", data.token, socket)) {
			dbase.adminGetPresenters(socket.id, function(data) {
				socket.emit("admin:presenters", data);
				console.log("[%s] [Socket] [action=admin:presenters] Outgoing".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:presenters] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:sessions", function(data) {
		console.log("[%s] [Socket] [action=admin:sessions] Incoming".debug, socket.id);
		if (session.isValid("admin:sessions", data.token, socket)) {
			dbase.adminGetSessions(socket.id, function(data) {
				socket.emit("admin:sessions", data);
				console.log("[%s] [Socket] [action=admin:sessions] Outgoing".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:sessions] Token invalid".warn, socket.id);
		}
	});
	socket.on("user:contact-delete", function(data) {
		console.log("[%s] [Socket] [action=user:contact-delete] Incoming".debug, socket.id);
		dbase.deleteContact(socket.id, data, function(data) {
			socket.emit("user:contacts", data);
			console.log("[%s] [Socket] [action=user:contacts] Outgoing".debug, socket.id);
		});
	});
	socket.on("admin:user-delete", function(data) {
		console.log("[%s] [Socket] [action=admin:user-delete] Incoming".info, socket.id);
		if (session.isValid("admin:user-delete", data.token, socket)) {
			dbase.adminDeleteUser(socket.id, data, function(data) {
				io.to('admins').emit("admin:users", data);
				console.log("[%s] [Socket] [action=admin:users] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.adminGetPresenters(socket.id, function(data) {
				io.to('admins').emit("admin:presenters", data);
				console.log("[%s] [Socket] [action=admin:presenters] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.publicGetPresenters(socket.id, function(data) {
				socket.broadcast.emit("public:presenters", data);
				console.log("[%s] [Socket] [action=public:presenters] Broadcast".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:user-delete] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:admin-delete", function(data) {
		console.log("[%s] [Socket] [action=admin:admin-delete] Incoming".info, socket.id);
		if (session.isValid("admin:admin-delete", data.token, socket)) {
			dbase.adminDeleteAdmin(socket.id, data, function(data) {
				io.to('admins').emit("admin:admins", data);
				console.log("[%s] [Socket] [action=admin:admins] [room=admins] Broadcast".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:admin-delete] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:session-delete", function(data) {
		console.log("[%s] [Socket] [action=admin:session-delete] Incoming".info, socket.id);
		if (session.isValid("admin:session-delete", data.token, socket)) {
			dbase.adminDeleteSession(socket.id, data, function(data) {
				io.to('admins').emit("admin:sessions", data);
				console.log("[%s] [Socket] [action=admin:sessions] [room=admins] Broadcast".debug, socket.id);
			});
			// force update of calendar to all users
			dbase.getCalendar(socket.id, function(data) {
				socket.broadcast.emit("public:sessions", data);
				console.log("[%s] [Socket] [action=public:sessions] Broadcast".debug, socket.id);
			});
			// send invalidate pinned sessions event to users
			socket.broadcast.emit("user:sessions-pinend-invalid");
			console.log("[%s] [Socket] [action=user:sessions-pinend-invalid] Broadcast".debug, socket.id);
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:session-delete] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:user-save", function(data) {
		console.log("[%s] [Socket] [action=admin:user-save] Incoming".info, socket.id);
		if (session.isValid("admin:user-save", data.token, socket)) {
			dbase.adminAddUser(socket.id, data, function(data) {
				io.to('admins').emit("admin:users", data);
				console.log("[%s] [Socket] [action=admin:users] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.adminGetPresenters(socket.id, function(data) {
				io.to('admins').emit("admin:presenters", data);
				console.log("[%s] [Socket] [action=admin:presenters] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.publicGetPresenters(socket.id, function(data) {
				socket.broadcast.emit("public:presenters", data);
				console.log("[%s] [Socket] [action=public:presenters] Broadcast".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:user-save] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:admin-save", function(data) {
		console.log("[%s] [Socket] [action=admin:admin-save] Incoming".info, socket.id);
		if (session.isValid("admin:admin-save", data.token, socket)) {
			dbase.adminAddAdmin(socket.id, data, function(data) {
				io.to('admins').emit("admin:admins", data);
				console.log("[%s] [Socket] [action=admin:admins] [room=admins] Broadcast".debug, socket.id);
			});
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:admin-save] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:session-save", function(data) {
		console.log("[%s] [Socket] [action=admin:session-save] Incoming".info, socket.id);
		if (session.isValid("admin:session-save", data.token, socket)) {
			dbase.adminAddSession(socket.id, data, function(data) {
				io.to('admins').emit("admin:sessions", data);
				console.log("[%s] [Socket] [action=admin:sessions] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.getCalendar(socket.id, function(data) {
				socket.broadcast.emit("public:sessions", data);
				console.log("[%s] [Socket] [action=public:sessions] Broadcast".debug, socket.id);
			});
			socket.broadcast.emit("user:sessions-pinend-invalid");
			console.log("[%s] [Socket] [action=user:sessions-pinend-invalid] Broadcast".debug, socket.id);
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:session-save] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:session-edit", function(data) {
		console.log("[%s] [Socket] [action=admin:session-edit] Incoming".info, socket.id);
		if (session.isValid("admin:session-edit", data.token, socket)) {
			dbase.adminEditSession(socket.id, data, function(data) {
				io.to('admins').emit("admin:sessions", data);
				console.log("[%s] [Socket] [action=admin:sessions] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.getCalendar(socket.id, function(data) {
				socket.broadcast.emit("public:sessions", data);
				console.log("[%s] [Socket] [action=public:sessions] Broadcast".debug, socket.id);
			});
			socket.broadcast.emit("user:sessions-pinend-invalid");
			console.log("[%s] [Socket] [action=user:sessions-pinend-invalid] Broadcast".debug, socket.id);
		} else {
			socket.emit("admin:session-expired", {});
			console.log("[%s] [Socket] [action=admin:session-edit] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:user-edit", function(data) {
		console.log("[%s] [Socket] [action=admin:user-edit] Incoming".info, socket.id);
		if (session.isValid("admin:user-edit", data.token, socket)) {
			dbase.adminEditUser(socket.id, data, function(data) {
				io.to('admins').emit("admin:users", data);
				console.log("[%s] [Socket] [action=admin:users] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.adminGetPresenters(socket.id, function(data) {
				io.to('admins').emit("admin:presenters", data);
				console.log("[%s] [Socket] [action=admin:presenters] [room=admins] Broadcast".debug, socket.id);
			});
			dbase.publicGetPresenters(socket.id, function(data) {
				socket.broadcast.emit("public:presenters", data);
				console.log("[%s] [Socket] [action=public:presenters] Broadcast".debug, socket.id);
			});
		} else {
			socket.emit("session-expired", {});
			console.log("[%s] [Socket] [action=admin:user-edit] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:admin-edit", function(data) {
		console.log("[%s] [Socket] [action=admin:admin-edit] Incoming".info, socket.id);
		if (session.isValid("admin:admin-edit", data.token, socket)) {
			dbase.adminEditAdmin(socket.id, data, function(data) {
				io.to('admins').emit("admin:admins", data);
				console.log("[%s] [Socket] [action=admin:admins] [room=admins] Broadcast".debug, socket.id);
			});
		} else {
			socket.emit("session-expired", {});
			console.log("[%s] [Socket] [action=admin:admin-edit] Token invalid".warn, socket.id);
		}
	});
	socket.on("admin:login", function(data) {
		console.log("[%s] [Socket] [action=admin:login] Incoming".auth, socket.id);
		dbase.adminLogin(socket.id, data, function(success) {
			if (success) {
				var sessionId = session.create(socket);
				socket.emit("admin:login", sessionId);
				console.log("[%s] [Socket] [action=admin:login] Outgoing [token=%s]".auth, socket.id, sessionId);
			} else {
				socket.emit("admin:login-failed", {});
				console.log("[%s] [Socket] [action=admin:login-failed] [user=%s] Outgoing".warn, socket.id, data.user);
			}
		});
	});
	socket.on("admin:logout", function(data) {
		console.log("[%s] [Socket] [action=admin:logout] Incoming".auth, socket.id);
		session.destroy(socket, data);
	});
	socket.on("user:contact-save", function(data) {
		console.log("[%s] [Socket] [action=user:contact-save] Incoming".debug, socket.id);
		// search if contact already exists
		dbase.saveContact(socket.id, data, function(data) {
			socket.emit("user:contacts", data);
			console.log("[%s] [Socket] [action=user:contacts] Outgoing".debug, socket.id);
		});
	});
	socket.on("user:contacts", function(data) {
		console.log("[%s] [Socket] [action=user:contacts] Incoming".debug, socket.id);
		dbase.getSavedContacts(socket.id, data.ownerid, function(data) {
			socket.emit("user:contacts", data);
			console.log("[%s] [Socket] [action=user:contacts] Outgoing".debug, socket.id);
		});
	});
});
// Start the actual server
server.listen(8080, "0.0.0.0");
console.log("[System] Server started".info);